package com.example.shruti.assignment6.util;

/**
 * Created by Shruti on 2/6/2015.
 */
public interface AppConstants {


    public static final String KEY_ROLL="roll";
    public static final String KEY_NAME="name";
    public static final String DATABASE_NAME="DetailsDb";
    public static final String DATABASE_TABLE="stuInfo";
    public static final int DATABASE_VERSION=2;
}
