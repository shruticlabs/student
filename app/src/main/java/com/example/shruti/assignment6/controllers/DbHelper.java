package com.example.shruti.assignment6.controllers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.shruti.assignment6.util.AppConstants;


public class DbHelper extends SQLiteOpenHelper implements AppConstants {
  //  DbHelper ourHelper;
    //Context ourContext;
    //SQLiteDatabase ourDatabase;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+DATABASE_TABLE+" ("+KEY_ROLL+" INTEGER PRIMARY KEY , "+KEY_NAME+" TEXT NOT NULL);"



        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABLE);
        onCreate(db);

    }

    
}
