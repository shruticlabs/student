package com.example.shruti.assignment6.entities;

import java.io.Serializable;

/**
 * Created by Shruti on 1/22/2015.
 */
public class Student{

    String name;
    int roll_no;


    public Student(int roll_no, String name) {
        this.roll_no = roll_no;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(int roll_no) {
        this.roll_no = roll_no;
    }
}
