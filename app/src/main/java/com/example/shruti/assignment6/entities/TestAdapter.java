package com.example.shruti.assignment6.entities;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.shruti.assignment6.R;

import java.util.List;


public class TestAdapter extends BaseAdapter {

    public List<Student> stuList;
    Context ctx;
    TextView tv;

    public TestAdapter(List<Student> stuList, Context cxt) {
        this.stuList = stuList;
        this.ctx = cxt;
    }

    @Override
    public int getCount() {
        return stuList.size();
    }

    @Override
    public Object getItem(int position) {
        return stuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.single_row,null);
        TextView tv = (TextView)view.findViewById(R.id.display1);
        tv.setText( stuList.get(position).name);
        TextView tv1 = (TextView)view.findViewById(R.id.display2);
        tv1.setText( stuList.get(position).roll_no+"");
        return view;
    }
}
