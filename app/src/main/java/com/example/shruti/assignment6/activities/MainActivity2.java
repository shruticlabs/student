package com.example.shruti.assignment6.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.shruti.assignment6.R;
import com.example.shruti.assignment6.controllers.DbController;
import com.example.shruti.assignment6.entities.Student;

import java.io.Serializable;


public class MainActivity2 extends Activity {


    EditText Edit1;
    EditText Edit2;

    Intent intent;
    int receivePosition;
    boolean caseEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
        Button saveBtn= (Button)findViewById(R.id.save);
        Button cancelBtn=(Button)findViewById(R.id.cancel);
        Edit1 = (EditText) findViewById(R.id.ed1);
        Edit2 = (EditText) findViewById(R.id.ed2);
        intent = this.getIntent();
        Bundle receiveBundle = this.getIntent().getExtras();
        if (receiveBundle != null) {
            caseEdit = true;
            final String receiveValue1 = receiveBundle.getString("name");
            Edit1.setText(String.valueOf(receiveValue1));
            final int receiveValue2 = receiveBundle.getInt("roll");
            Edit2.setText(Integer.toString(receiveValue2));
            receivePosition = receiveBundle.getInt("position");
        }
        String name= intent.getStringExtra("name");
        int roll=intent.getIntExtra("roll",0);
        if(name!=null&&(roll!=0)) {
            saveBtn.setEnabled(false);
            cancelBtn.setEnabled(false);
            EditText nameDisplay = (EditText) findViewById(R.id.ed1);
            EditText rollDisplay = (EditText) findViewById(R.id.ed2);
            nameDisplay.setText(name);
            rollDisplay.setText(String.valueOf(roll));
        }
    }


    public void click2(View view) {
        switch (view.getId()) {

            case R.id.save:

                int roll = Integer.parseInt(Edit2.getText().toString());
                String name = Edit1.getText().toString();
                new LoadData(roll, name).execute(caseEdit ? "edit" : "save");


                intent.putExtra("name", name);
                intent.putExtra("roll", roll);
                intent.putExtra("position", receivePosition);
                setResult(RESULT_OK, intent);
                finish();

            case R.id.cancel:
                setResult(RESULT_CANCELED);
                finish();

        }


    }

    public class LoadData extends AsyncTask<String, Integer, String> {

        int roll;
        String name;

        public LoadData(int Roll, String Name) {
            roll = Roll;
            name = Name;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected String doInBackground(String... params) {

            DbController entry = new DbController(MainActivity2.this);
            entry.open();
            long l=Long.parseLong(String.valueOf(roll));
            if (params[0].equals("save")) {
                entry.createEntry(roll, name);
            } else {
                entry.update(roll,name,l);
            }


            entry.close();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}
