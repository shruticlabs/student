package com.example.shruti.assignment6.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.shruti.assignment6.R;
import com.example.shruti.assignment6.controllers.DbController;
import com.example.shruti.assignment6.entities.Student;
import com.example.shruti.assignment6.entities.TestAdapter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {


    ListView listView;
    TestAdapter adapter;
    List<Student> stuList=new ArrayList<Student>();

   static public String name;
     static public   int roll;
    Context context=this;
    boolean caseEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView=(ListView)findViewById(R.id.list_view);

        DbController db=new DbController(context);
        db.open();
        stuList=db.getData();
        db.close();
        adapter = new TestAdapter(stuList,this);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
                LayoutInflater factory = LayoutInflater.from(context);
                final View DialogView = factory.inflate(
                        R.layout.dialog, null);
                final AlertDialog dialog1 = new AlertDialog.Builder(MainActivity.this).create();
                dialog1.setView(DialogView);




                DialogView.findViewById(R.id.view_btn).setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        String name=  adapter.stuList.get(position).getName();
                        int roll=adapter.stuList.get(position).getRoll_no();

                        new LoadData(roll, name).execute(caseEdit ? "delete" : "view");
                        dialog1.dismiss();



                    }
                });
                DialogView.findViewById(R.id.edit_btn).setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view){
                        if (view.getId()>0) {

                            String valueString=  adapter.stuList.get(position).getName();
                            int valueInt=adapter.stuList.get(position).getRoll_no();
                            Bundle sendBundle = new Bundle();
                            sendBundle.putString("name", valueString);
                            sendBundle.putInt("roll", valueInt);
                            sendBundle.putInt("position", position);

                            Intent intent;
                            intent = new Intent(MainActivity.this, MainActivity2.class);
                            intent.putExtras(sendBundle);

                            startActivityForResult(intent, 1);

                            dialog1.dismiss();
                        }
                    }
                });
                DialogView.findViewById(R.id.delete_btn).setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        String name=  adapter.stuList.get(position).getName();
                        int roll=adapter.stuList.get(position).getRoll_no();

                       caseEdit=true;
                        new LoadData(roll,name).execute(caseEdit ?  "delete" : "view");
                        adapter.stuList.remove(position);
                        adapter.notifyDataSetChanged();


                        dialog1.dismiss();

                    }


                });

                dialog1.show();

            }
        });
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {

                name = data.getStringExtra("name");
                roll = data.getIntExtra("roll", 0);
                adapter.stuList.add(new Student(roll, name));
                adapter.notifyDataSetChanged();

            } else if (resultCode == RESULT_CANCELED) {

            }

        }
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

                Bundle extras = data.getExtras();
             int position=extras.getInt("position");
                adapter.stuList.remove(position);
                name = extras.getString("name");
                roll = extras.getInt("roll", 0);
                adapter.stuList.add(new Student(roll, name));
                adapter.notifyDataSetChanged();


            }else if (resultCode == RESULT_CANCELED) {

            }

        }
    }

    public void click(View view) {

        switch(view.getId()){
            case R.id.Add_button:
                Intent intent = new Intent(this,MainActivity2.class);

                startActivityForResult(intent, 100);


                break;

            default:
                break;


        }



    }
    public class LoadData extends AsyncTask<String, Integer, String> {

        int roll;
        String name;

        public LoadData(int Roll, String Name) {
            roll = Roll;
            name = Name;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected String doInBackground(String... params) {

            DbController entry = new DbController(MainActivity.this);
            entry.open();
            long l=Long.parseLong(String.valueOf(roll));
            if (params[0].equals("delete")) {
                try {
                    entry.deleteEntry(l);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
            Student stu =  entry.getSingleRow(roll);
                String name=stu.getName();
                int roll=stu.getRoll_no();
               Intent intent=new Intent(MainActivity.this,MainActivity2.class);
                intent.putExtra("name", name);
                intent.putExtra("roll",roll);
               startActivity(intent);

            }


            entry.close();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

}
