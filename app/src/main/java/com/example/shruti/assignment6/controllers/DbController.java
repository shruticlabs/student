package com.example.shruti.assignment6.controllers;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.shruti.assignment6.entities.Student;
import com.example.shruti.assignment6.util.AppConstants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DbController implements AppConstants{

    DbHelper ourHelper;
    Context ourContext;
    SQLiteDatabase ourDatabase;

    public DbController(Context c)
    {
        ourContext=c;


    }
    public DbController open()
    {
       ourHelper=new DbHelper(ourContext);
       ourDatabase=ourHelper.getWritableDatabase();
        return this;
    }
    public void close()

    {

        ourHelper.close();
    }


    public long createEntry(int roll, String name) {
        ContentValues cv=new ContentValues();
        cv.put(KEY_ROLL,roll);
        cv.put(KEY_NAME,name);
       return ourDatabase.insert(DATABASE_TABLE,null,cv);


    }

    public long update(int roll,String name,long rollNum) {
        ContentValues cvUpdate=new ContentValues();
        cvUpdate.put(KEY_ROLL,roll);
        cvUpdate.put(KEY_NAME,name);
        return ourDatabase.update(DATABASE_TABLE,cvUpdate,KEY_ROLL + "=" +rollNum,null);

    }

    public void deleteEntry(long rollNum) throws SQLException{

      // ourDatabase.delete(DATABASE_TABLE,KEY_ROLL+ "=" +Long.toString(rollNum),null);
       ourDatabase.delete(DATABASE_TABLE,KEY_ROLL+ "=?",new String[]{Long.toString(rollNum)});
    }

    public List<Student> getData() {
        List<Student> list=new ArrayList<>();
        String[] columns = new String[]{KEY_ROLL, KEY_NAME};
        Cursor c = ourDatabase.query(DATABASE_TABLE, columns, null, null, null, null, null);
        String result = "";

        int iName = c.getColumnIndex(KEY_NAME);
        int iRoll = c.getColumnIndex(KEY_ROLL);
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            list.add(new Student(Integer.parseInt(c.getString(iRoll)),c.getString(iName)));
           // result = result + "" + c.getString(iRoll) + "" + c.getString(iRoll) + "\n";
        }
        return list;
    }


    public Student getSingleRow(int roll){
        Cursor cursor = ourDatabase.query(DATABASE_TABLE, new String[] { KEY_ROLL,
                        KEY_NAME }, KEY_ROLL + "=?",
                new String[] { String.valueOf(roll) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Student stu = new Student(Integer.valueOf(cursor.getString(0)),cursor.getString(1));

        return stu;


    }
}
