package com.example.shruti.assignment6.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shruti.assignment6.R;
import com.example.shruti.assignment6.activities.MainActivity;
import com.example.shruti.assignment6.entities.AlertDialogManager;
import com.example.shruti.assignment6.entities.SessionManager;

import static com.example.shruti.assignment6.R.id.editPassword;


public class Login extends ActionBarActivity {
    EditText txtUsername, txtPassword;


    Button btnLogin;


    AlertDialogManager alert = new AlertDialogManager();


    SessionManager session;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        // Session Manager
        session = new SessionManager(getApplicationContext());

        // Email, Password input text
        txtUsername = (EditText) findViewById(R.id.editName);
        txtPassword = (EditText) findViewById(editPassword);

        Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), Toast.LENGTH_LONG).show();



        btnLogin = (Button) findViewById(R.id.login1);



        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                String username = txtUsername.getText().toString();
                String password = txtPassword.getText().toString();


                if(username.trim().length() > 0 && password.trim().length() > 0){

                    if(username.equals("shruti") && password.equals("12345")){


                        session.createLoginSession("Assignment6", "shruti@clicklabs.in");


                        Intent i = new Intent(Login.this, MainActivity.class);
                        startActivity(i);
                        finish();

                    }else{

                        alert.showAlertDialog(Login.this, "Login failed..", "Username/Password is incorrect", false);
                    }
                }else{

                    alert.showAlertDialog(Login.this, "Login failed..", "Please enter username and password", false);
                }

            }
        });
    }
}












